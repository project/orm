<?php
/**
 * Constructs orm query based on input criteria
 * 
 * @author BrendonCrawford
 * @created 2009-11-08
 * @see http://drupal.org/project/orm
 *
 */
class OrmFinder {

  const TABLE_TYPE_CCK = 1;
  const TABLE_TYPE_NODE = 2;

  protected $type = NULL;
  protected $name = NULL;
  public $wheres = NULL;
  public $fields = NULL;
  public $froms = NULL;
  public $joins = NULL;
  
  /**
   * 
   * @constructor
   * @param $type String
   * @param $name String
   * @param $conds Assoc
   */
  public function __construct($type, $name, $conds) {
    $this->type = $type;
    $this->name = $name;
    $this->wheres = $this->buildWheres($conds);
    $this->fields = $this->buildFields();
    $this->froms = $this->buildFroms();
    $this->joins = $this->buildJoins($this->froms, $this->wheres);
  }

  /**
   * Builds joins
   * 
   * @param $froms Array
   * @param $wheres Array
   * @return Array
   */
  private function buildJoins($froms, $wheres) {
    $joins = array();
    foreach ($wheres as $where) {
      // Dont need to join tables already in from
      if (!in_array($where, $froms)) {
        // For now only need to join on CCK tables
        if ($where->tableType === self::TABLE_TYPE_CCK) {
          $joins[] = $this->addJoin($where->table, $this->getCckJoiner(),
            $froms[0]->table, $froms[0]->pk);
        }
      }
    }
    return $joins;
  }

  /**
   * Gets fieldname for joing CCK with nodes
   * 
   * @return String
   */
  private function getCckJoiner() {
    if (OrmUtil::dver(7)) {
      return 'entity_id';
    }
    elseif (OrmUtil::dver(6)) {
      return 'nid';
    }
  }

  /**
   * REturns an join oject
   * @param $srcTable String
   * @param $srcField String
   * @param $dstTable String
   * @param $dstField String
   * @return Object
   */
  private function addJoin($srcTable, $srcField, $dstTable, $dstField) {
    return (object)array(
      'srcTable' => $srcTable,
      'srcField' => $srcField,
      'dstTable' => $dstTable,
      'dstField' => $dstField
    );
  }

  /**
   * Builds froms
   * 
   * @return Array
   */
  private function buildFroms() {
    $froms = array();
    $froms[] = $this->addFrom('node', 'nid');
    return $froms;
  }

  /**
   * Adds a from
   * 
   * @param $table String
   * @param $pk String
   * @return Object
   */
  private function addFrom($table, $pk) {
    return (object)array(
      'table' => $table,
      'pk' => $pk
    );
  }

  /**
   * Builds fields
   * 
   * @return Array
   */
  private function buildFields() {
    $fields = array();
    $fields[] = $this->addField('node', '*');
    return $fields;
  }

  /**
   * Adds a field
   * 
   * @param $table String
   * @param $field String
   * @return Object
   */
  private function addField($table, $field) {
    return (object)array(
      'table' => $table,
      'field' => $field
    );
  }

  /**
   * Builds wheres
   * 
   * @param $conds Assoc
   * @return Array
   */
  private function buildWheres($conds) {
    $schemaInst = OrmSchema::find();
    $where = array();
    $where[] = $this->addWhere('node', 'type', $this->type,
      self::TABLE_TYPE_NODE, OrmQueryBuilder::COMP_TYPE_EQUALITY);
    foreach ($conds as $ck => $cv) {
      list($table, $field) = explode('.', $ck);
      $fieldParts = explode('_', $field);
      // If CCK
      if ($schemaInst->isNode($table, $field)) {
        $table = 'node';
        $tableType = self::TABLE_TYPE_NODE;
      }
      elseif ($schemaInst->isCCK($table, $field)) {
        $table = $this->buildCckTableRef($fieldParts);
        $tableType = self::TABLE_TYPE_CCK;
        $field = $schemaInst->getCCKCode($table, $field);
      }
      $where[] = $this->addWhere($table, $field, $cv,
        $tableType, OrmQueryBuilder::COMP_TYPE_EQUALITY);
    }
    return $where;
  }

  /**
   * Builds cck table refs
   * 
   * @param $fieldParts Array
   * @return String
   */
  private function buildCckTableRef($fieldParts) {
    // D7
    if (OrmUtil::dver(7)) {
      $table =
        implode('_', array_merge(array('field', 'data'), $fieldParts));
    }
    // D6
    elseif (OrmUtil::dver(6)) {
      $table =
        implode('_', array('content', 'type', $this->type));
    }
    return $table;
  }

  /**
   * Adds a where
   * 
   * @param $table String
   * @param $field String
   * @param $value String
   * @param $tableType Int
   * @param $comparisonType String
   * @return Object
   */
  private function addWhere($table, $field, $value,
      $tableType, $comparisonType) {
    return (object)array(
      'table' => $table,
      'field' => $field,
      'value' => $value,
      'tableType' => $tableType,
      'comparisonType' => $comparisonType
    );
  }

}
