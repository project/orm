<?php
/**
 * Builds a query
 * 
 * @author BrendonCrawford
 * @created 2009-11-08
 * @see http://drupal.org/project/orm
 *
 */

class OrmQueryBuilder {

  const COMP_TYPE_EQUALITY = 1;

  public $compMap = array(
    self::COMP_TYPE_EQUALITY => '='
  );

  public $sql = NULL;
  private $froms = NULL;
  private $fields = NULL;
  private $joins = NULL;
  private $wheres = NULL;
  
  /**
   * @constructor
   * @param  $finder OrmFinder
   */
  public function __construct($finder) {
    $fields = $this->buildFields($finder->fields);
    $froms = $this->buildFroms($finder->froms);
    $joins = $this->buildJoins($finder->joins);
    $wheres = $this->buildWheres($finder->wheres, $args);
    $this->sql = $this->buildSQL($fields, $froms, $joins, $wheres);
    $this->args = $args;
  }

  /**
   * Puts it all together
   * 
   * @param $fields Array
   * @param $froms Array
   * @param $joins Array
   * @param $wheres Array
   * @return String
   */
  public function buildSQL($fields, $froms, $joins, $wheres) {
    $out = implode("\n", array(
      $fields,
      $froms,
      $joins,
      $wheres
    ));
    return $out;
  }

  /**
   * Builds froms
   * 
   * @param $froms Array
   * @return String
   */
  public function buildFroms($froms) {
    $q  = 'FROM';
    $q .= ' ';
    $i = array();
    foreach ($froms as $from) {
      $i[] = sprintf('{%s}', $from->table);
    }
    $q .= implode(',', $i);
    return $q;
  }

  /**
   * Builds fields
   * 
   * @param $fields Array
   * @return String
   */
  public function buildFields($fields) {
    $q  = 'SELECT';
    $q .= ' ';
    $i = array();
    foreach ($fields as $field) {
      $i[] = sprintf('{%s}.%s', $field->table, $field->field);
    }
    $q .= implode(',', $i);
    return $q;
  }

  /**
   * Builds joins
   * 
   * @param $joins Array
   * @return String
   */
  public function buildJoins($joins) {
    $q  = '';
    $i = array();
    foreach ($joins as $join) {
      $i[] =
        sprintf('INNER JOIN {%s} ON {%s}.%s = {%s}.%s',
          $join->srcTable, $join->srcTable,
          $join->srcField, $join->dstTable,
          $join->dstField);
    }
    $q .= implode(' ', $i);
    return $q;
  }

  /**
   * Builds wheres
   * 
   * @param $wheres Array
   * @param $args Array Reference
   * @return String
   */
  public function buildWheres($wheres, &$args) {
    $q  = 'WHERE';
    $q .= ' ';
    $i = array();
    $args = array();
    foreach ($wheres as $where) {
      $i[] =
        sprintf($this->createWhereClause($where),
          $where->table, $where->field,
          $this->compMap[$where->comparisonType]);
      $args[] = $where->value;
    }
    $q .= implode(' AND ', $i);
    return $q;
  }

  /**
   * Creates a single where clause
   * 
   * @param $where Object
   * @return String
   */
  private function createWhereClause($where) {
    if (OrmUtil::dver(7)) {
      $w = "{%s}.%s %s ?";
    }
    elseif (OrmUtil::dver(6)) {
      $w  = "{%s}.%s %s ";
      $w .= is_numeric($where->value) ? "%%d" : "'%%s'";
    }
    return $w;
  }

}