<?php
/**
 * Constructs node schema
 * 
 * @author BrendonCrawford
 * @created 2009-11-08
 * @see http://drupal.org/project/orm
 *
 */

class OrmSchema {
  
  private static $instance = NULL;
  private $schema = NULL;
  
  /**
   * @constructor
   */
  private function __construct() {
    $c = __CLASS__;
    if (self::$instance !== NULL && (self::$instance instanceof $c)) {
      throw new Exception('Only one instance of this class can be created');
    }
  }

  /**
   * Returns schema
   * 
   * @return Assoc
   */
  public function getSchema() {
    return $this->schema;
  }
  
  /**
   * Returns sub-schema for a table
   * 
   * @param $table String
   * @return Assoc
   */
  public function getTableSchema($table) {
    return $this->schema[$table];
  }
  
  /**
   * Builds schema
   * 
   * @return Bool
   */
  public function setSchema() {
    $types_q =
      "SELECT nt.type " .
      "FROM {node_type} nt ";
    $node_fields_q =
      "DESCRIBE {node} ";
    $fields_q =
      "SELECT " .
      "  cnf.field_name, " .
      "  cnf.type, " .
      "  cnf.db_columns, " .
      "  cnf.global_settings " .
      "FROM {content_node_field} cnf ";
    $insts_q =
      "SELECT " .
      "  cnfi.field_name, " .
      "  cnfi.type_name " .
      "FROM {content_node_field_instance} cnfi ";
    $types_r = db_query($types_q);
    $fields_r = db_query($fields_q);
    $insts_r = db_query($insts_q);
    $node_fields_r = db_query($node_fields_q);
    $fields = array();
    $insts = array();
    $schema = array();
    $node_fields = array();
    while ($node_field = db_fetch_object($node_fields_r)) {
      $nf = (object) array();
      $nf->schema_type = 'node';
      $nf->field_name = $node_field->Field;
      $nf->type = $node_field->Type;
      $node_fields[$node_field->Field] = $nf;
    }
    while ($field = db_fetch_object($fields_r)) {
      $f = (object) array();
      $f->schema_type = 'cck';
      $f->field_name = $field->field_name;
      $f->type = $field->type;
      $f->columns = unserialize($field->db_columns);
      $f->settings = unserialize($field->global_settings);
      $f->column_key = key($f->columns);
      $f->code = implode('_', array($field->field_name, $f->column_key));
      $fields[$field->field_name] = $f;
    }
    while ($inst = db_fetch_object($insts_r)) {
      $insts[] = $inst;
    }
    while ($type = db_fetch_object($types_r)) {
      $o = array();
      foreach ($insts as $inst_v) {
        if ($inst_v->type_name === $type->type) {
          if (array_key_exists($inst_v->field_name, $fields)) {
            $fname = $inst_v->field_name;
            $o[$fname] = $fields[$inst_v->field_name];
          }
        }
      }
      foreach ($node_fields as $node_field_v) {
        $fname = $node_field_v->field_name;
        $o[$fname] = $node_field_v;
      }
      $schema[OrmUtil::reverseType($type->type)] = $o;
    }
    //orm_debug($fields, TRUE);
    //orm_debug($insts, TRUE);
    //orm_debug($schema, TRUE);
    $this->schema = $schema;
  }
  
  /**
   * Gets a cck code
   * 
   * @param $table String
   * @param $field String
   * @return String
   */
  public function getCCKCode($table, $field) {
    return $this->schema[$table][$field]->code;
  }
  
  /**
   * Detemines if is node type
   * 
   * @param $table String
   * @param $field String
   * @return Bool
   */
  public function isNode($table, $field) {
    return $this->isType($table, $field, 'node');
  }

  /**
   * Determines if is cck type
   * 
   * @param $table String
   * @param $field String
   * @return Bool
   */
  public function isCCK($table, $field) {
    return $this->isType($table, $field, 'cck');
  }

  /**
   * Gets type
   * 
   * @param $table String
   * @param $field String
   * @param $type String
   * @return Bool
   */
  public function isType($table, $field, $type) {
    if (array_key_exists($table, $this->schema)) {
      if (array_key_exists($field, $this->schema[$table])) {
        return ($this->schema[$table][$field]->schema_type === $type);
      }
    }
    return FALSE;
  }

  /**
   * Finds singletons instance
   * 
   * @return OrmSchema
   */
  public static function find() {
    $c = __CLASS__;
    if (self::$instance === NULL || !(self::$instance instanceof $c)) {
      self::$instance = new $c;
    }
    return self::$instance;
  } 

}