<?php
/**
 * Handles model for a node type
 * 
 * @author BrendonCrawford
 * @created 2009-11-08
 * @see http://drupal.org/project/orm
 *
 */

class OrmTypeModel {

  const FIELD_TYPE_TEXT = 0;
  const FIELD_TYPE_NODEREF = 1;

  protected $type = NULL;
  protected $name = NULL;
  protected $clsName = NULL;
  protected $schema = NULL;

  /**
   * @constructor
   * @param $name String
   */
  private function __construct($name) {
    //print "<pre>";
    //print_r(OrmSchema::find()->getSchema());
    //print "</pre>";
    $this->type = OrmUtil::convertType($name);
    $this->name = $name;
    $this->clsName = $this->convertClsName($name);
    $this->schema = OrmSchema::find()->getTableSchema($this->name);
  }

  /*
   * Sets up classname
   * 
   * @param $name String
   */
  private function convertClsName($name) {
    return ($name . 'Node');
  }

  /**
   * Retrieves a model type object
   * 
   * @param $type String
   * @return OrmTypeModel
   */
  public static function init($type) {
    static $models = array();
    if (!array_key_exists($type, $models)) {
      $models[$type] = new OrmTypeModel($type);
    }
    return $models[$type];
  }

  /**
   * Finds nodes
   * 
   * @param $conds Assoc
   * @param $opts Assoc
   * @return Array
   */
  public function find($conds, $opts=array()) {
    $opts = array_merge(array(
      'fullNode' => TRUE
    ), $opts);
    $finder = new OrmFinder($this->type, $this->name, $conds);
    $query = new OrmQueryBuilder($finder);
    $results = OrmUtil::exec($query->sql, $query->args);
    $out = $this->processFindResults($results, $opts);
    return $out;
  }

  /**
   * Finds a node by id
   * 
   * @param $nid Int
   * @param $opts Assoc
   * @return Array
   */
  public function findById($nid, $opts=array()) {
    $opts = array_merge(array(
      'fullNode' => TRUE
    ), $opts);
    $results = array(
      (object) array(
        'nid' => (int)$nid
      )
    );
    $out = $this->processFindResults($results, $opts);
    return $out;
  }

  /**
   * Creates a node
   * 
   * @param $props Assoc
   * @return OrmNode
   */
  public function create($props=array()) {
    if (OrmUtil::dver(7)) {
      throw new Exception('create() is not yet implemented for Drupal 7');
      return FALSE;
    }
    elseif (OrmUtil::dver(6)) {
      $node = new stdClass;
      $node->type = $this->type;
      $node->uid = 1;
      $node->title = '';
      node_save($node);
      $newNode = $this->buildNode($node);
      return $newNode;
    }
  }

  /**
   * Loads up the node class if not loaded already
   * 
   * @return Bool
   */
  private function loadNodeClass() {
    if (!class_exists($this->clsName)) {
      eval(sprintf('class %s extends OrmNode {};', $this->clsName));
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Handles recursive nodereference loading
   * 
   * @param $nodeProp Assoc
   * @param $prop String
   * @param $opts Assoc
   * @return Bool
   */
  private function getRefs(&$nodeProp, $prop, $opts) {
    if (array_key_exists($prop, $this->schema)) {
      if ($this->schema[$prop]->schema_type === 'cck') {
        if ($this->schema[$prop]->type === 'nodereference') {
          for ($i = 0, $_i = sizeof($nodeProp); $i < $_i; $i++) {
          //if (is_array($val) && array_key_exists(0, $val)) {
            if (array_key_exists(
                $this->schema[$prop]->column_key, $nodeProp[$i])) {
              $nid = (int)$nodeProp[$i][$this->schema[$prop]->column_key];
              $this->nodeRef($nodeProp, $i, $nid, $prop, $opts);
            }
          }
        }
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Loads a nodereference
   * 
   * @param $nodeProp Assoc
   * @param $propIndex Int
   * @param $nid Int
   * @param $prop String
   * @param $opts Assoc
   * @return Bool
   */
  private function nodeRef(&$nodeProp, $propIndex, $nid, $prop, $opts) {
    $childNode = node_load($nid);
    if ($childNode !== NULL) {
      $childModelName = OrmUtil::reverseType($childNode->type);
      $childModel = self::init($childModelName);
      //$childPropName = OrmUtil::reverseType($prop);
      //$childPropName = 'node';
      $nodeProp[$propIndex] = $childModel->convertNode($childNode, $opts);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  
  /**
   * Converts standard Drupal node to a Node Object
   * 
   * @param $rawNode Object[DrupalNode]
   * @param $opts Assoc
   * @return OrmNode
   */
  private function convertNode($rawNode, $opts=array()) {
    $opts = array_merge(array(
      'fullNode' => FALSE
    ), $opts);
    if ($opts['fullNode']) {
      $newNode = node_load($rawNode->nid);
    }
    else {
      $newNode = $rawNode;
    }
    $node = $this->buildNode($newNode, $opts);
    return $node;
  }
  
  /**
   * Builds a node
   * 
   * @param $newNode Object[DrupalNode]
   * @param $opts Assoc
   * @return OrmNode
   */
  private function buildNode($newNode, $opts) {
    $this->loadNodeClass();
    $node = new $this->clsName($newNode, $opts);
    foreach ($node as $prop => $val) {
      $this->getRefs($node->{$prop}, $prop, $opts);
    }
    return $node;
  }

  /**
   * Handles a result set
   * 
   * @param $results MySQLResultSet
   * @param $opts Assoc
   * @return Array
   */
  private function processFindResults($results, $opts) {
    $out = array();
    foreach($results as $result) {
      $out[] = $this->convertNode($result, $opts);
    }
    return $out;
  }

}
