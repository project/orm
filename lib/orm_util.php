<?php
/**
 * Helper util funcionts
 * 
 * @author BrendonCrawford
 * @created 2009-11-08
 * @see http://drupal.org/project/orm
 *
 */

class OrmUtil {
  
  /**
   * Test for drupal version
   * 
   * @param $ver Int
   * @return Bool
   */
  public static function dver($ver) {
    $ver = (int)$ver;
    $crits = array(
      6 => 'CACHE_AGGRESSIVE',
      7 => 'ERROR_REPORTING_DISPLAY_ALL'
    );
    foreach ($crits as $k => $v) {
      if ($ver === $k && defined($v)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Executes SQL statement
   * 
   * @param $sql String
   * @param $args Array
   * @return MySQLResultSet
   */
  public static function exec($sql, $args=array()) {
    $out = array();
    $results = db_query($sql, $args);
    if (OrmUtil::dver(7)) {
      $out = $results;
    }
    elseif (OrmUtil::dver(6)) {
      while ($result = db_fetch_object($results)) {
        $out[] = $result;
      }
    }
    return $out;
  }
 
  /**
   * Converts ToggleCase to underscore_case
   * 
   * @param $name String
   * @return String
   */
  public static function convertType($name) {
    $out = '';
    $un = chr(95);
    $s = strlen($name);
    for($i = 0; $i < $s; $i++) {
      $o = ord($name[$i]);
      // Uppercase
      if ($o >= 65 && $o <= 90) {
        // Not first char
        if ($i > 0) {
          $out .= $un;
        }
        $out .= chr($o + 32);
      }
      // Lowercase/Numbers
      elseif (($o >= 97 && $o <= 122) || ($o >= 48 && $o <= 57)) {
        $out .= chr($o);
      }
    }
    return $out;
  }

  /**
   * Converts underscore_case to ToggleCase
   * 
   * @param $name String
   * @return String
   */
  public static function reverseType($name) {
    $out = '';
    $un = chr(95);
    $s = strlen($name);
    $newWord = TRUE;
    for($i = 0; $i < $s; $i++) {
      $o = ord($name[$i]);
      // Lowercase
      if (($o >= 97 && $o <= 122)) {
        if ($newWord) {
          $newWord = FALSE;
          $out .= chr($o - 32);
        }
        else {
          $out .= chr($o);
        }
      }
      // Underscore
      elseif ($o === 95) {
        $newWord = TRUE;
      }
      // Uppercase
      else {
        $out .= chr($o);
        $newWord = FALSE;
      }
    }
    return $out;
  }

}