<?php
/**
 * Basic super-class for all Node objects
 * 
 * @author BrendonCrawford
 * @created 2009-11-08
 * @see http://drupal.org/project/orm
 *
 */
class OrmNode extends stdClass {
  
  /**
   * @constructor
   * @param $node Object[DrupalNode]
   * @param $opts Assoc
   */
  public function __construct($node, $opts=array()) {
    $methods = get_class_methods($this);
    foreach($node as $prop => $val) {
      if (in_array($prop, $methods)) {
        $prop = '_' . $prop;
      }
      $this->{$prop} = $val;
    }
  }

  /**
   * Saves a node
   * 
   * @return unknown_type
   */
  public function save() {
    node_save($this);
  }

  /**
   * Delete
   * 
   * @return unknown_type
   */
  public function delete() {
    node_delete($this->nid);
  }

  /**
   * Sets title
   * 
   * @param $val String
   * @return Bool
   */
  public function setTitle($val) {
    if (OrmUtil::dver(7)) {
      $title =
        array(FIELD_LANGUAGE_NONE => array(array('value' => $val)));
    }
    elseif (OrmUtil::dver(6)) {
      $title = $val;
    }
    $this->title = $val;
    return TRUE;
  }

}